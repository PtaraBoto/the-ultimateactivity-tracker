class CreateActivities < ActiveRecord::Migration
  def change
    create_table :activities do |t|
      t.string :type
      t.string :title
      t.text :description
      t.datetime :startdatetime
      t.float :latstart
      t.float :longstart
      t.integer :profile_id

      t.timestamps null: false
    end
  end
end
