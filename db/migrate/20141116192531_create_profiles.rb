class CreateProfiles < ActiveRecord::Migration
  def change
    create_table :profiles do |t|
      t.string :name
      t.string :address
      t.string :gender
      t.date :birthday
      t.string :country
      t.text :description
      t.integer :team_id
      t.integer :user_id

      t.timestamps null: false
    end
  end
end
