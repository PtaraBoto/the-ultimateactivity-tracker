json.array!(@activities) do |activity|
  json.extract! activity, :id, :type, :title, :description, :startdatetime, :latstart, :longstart, :profile_id
  json.url activity_url(activity, format: :json)
end
