json.array!(@profiles) do |profile|
  json.extract! profile, :id, :name, :address, :gender, :birthday, :country, :description, :team_id, :user_id
  json.url profile_url(profile, format: :json)
end
