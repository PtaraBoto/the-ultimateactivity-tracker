class Activity < ActiveRecord::Base

  belongs_to :profile

  validates :profile_id, presence: true
  validates :type, presence: true
  validates :title, presence: true
  validates :profile_id, presence: true
end
