class Friendship < ActiveRecord::Base

  belongs_to :profile
  belongs_to :friend, :class_name => "Profile", :foreign_key =>"friend_id"

  validates :profile_id, presence: true
  validates :friend_id, presence: true
  validates :status, presence: true

  def self.accept(profile, friend)
        transaction do
          accept_one_side(profile, friend)
          accept_one_side(friend, profile)
    end
  end

  def self.accept_one_side(profile, friend)
    request = find_by_user_id_and_friend_id(profile, friend)
    request.status = 'accepted'
    request.save!
  end

  def self.add(profile, friend)
    if profile != friend and !find_by_profile_id_and_friend_id(profile, friend)
      transaction do
        create(:profile => profile, :friend => friend, :status => "pending")
        create(:profile => friend, :friend => profile, :profile => "requested")
      end
    end
  end

  def self.remove(profile, friend)
    transaction do
      remove_one_side(profile, friend)
      remove_one_side(friend, profile)
    end
  end

  def self.remove_one_side(profile, friend)
    request = find_by_user_id_and_friend_id(profile, friend)
    request.destroy
  end
end
