class FriendshipsController < ApplicationController
  before_action :set_friendship, only: [:show, :edit, :update, :destroy]

  respond_to :html

  def index
    @friendships = Friendship.all
    respond_with(@friendships)
  end

  def show
    #respond_with(@friendship)
  end

  def new
    #@friendship = Friendship.new
    #respond_with(@friendship)
  end

  def edit
  end

  def create
    #@friendship = Friendship.new(friendship_params)
    #@friendship.save
    #respond_with(@friendship)

    #@friend = Profile.find(params[:friend_id])
    @profile = Profile.find(current_user)
    @friend = Profile.find(params[:friend_id])
    @friendship = Friendship.add(@profile , @friend)

    if @friendship.nil?
      flash[:notice] = "Unable to add friend."
      redirect_to profile_path
    else
      flash[:error] = "Added friend."
      redirect_to home_index_path
    end
  end

  def update

    Friendship.accept(@friendship.profile, @friendship.friend)

    flash[:notice] = 'Friend sucessfully accepted!'
    redirect_to profile_path

  end

  def destroy
    Friendship.remove(@friendship.profile,@friendship.friend)
    flash[:notice] = 'Friend sucessfully removed!'
    redirect_to home_index_path

  end

  private
    def set_friendship
      @friendship = Friendship.find(params[:id])
    end

    def friendship_params
      params.require(:friendship).permit(:profile_id, :friend_id, :status)
    end
end
